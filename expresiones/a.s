
.globl fibonacci
.type fibonacci, @function
fibonacci:
	pushq %rbp
	movq %rsp, %rbp
	subq $104, %rsp
	movq %rdi, -8(%rbp)
	movq $0, %rdx
	movq %rdx, -24(%rbp)
	movq $1, %rdx
	movq %rdx, -32(%rbp)
	movq $1, %rdx
	movq %rdx, -16(%rbp)
.L2:
	movq -16(%rbp),  %rax
	movq -8(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -56(%rbp)
	movq $1, %rax
	movq -56(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L3
	movq -24(%rbp), %rax
	addq -32(%rbp), %rax
	movq %rax, -64(%rbp)
	movq -64(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq -40(%rbp), %rdx
	movq %rdx, -32(%rbp)
	movq -16(%rbp), %rax
	addq $1, %rax
	movq %rax, -72(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -16(%rbp)
jmp .L2
.L3:
	movq -8(%rbp),  %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -80(%rbp)
	movq -8(%rbp),  %rax
	movq $0,  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -88(%rbp)
	movq -80(%rbp), %rax
	movq -88(%rbp), %rbx
	or   %rax, %rbx
	movq %rbx, -96(%rbp)
	movq $0, %rax
	movq -96(%rbp),  %rbx
	cmp %rax, %rbx
	je .L4
	movq $0, %rax
	jmp .L1
	jmp .L5
.L4:
	movq -32(%rbp), %rax
	jmp .L1
.L5:
	movq $0, %rax
	jmp .L1
	.L1:
	leave
	ret

.globl factorial
.type factorial, @function
factorial:
	pushq %rbp
	movq %rsp, %rbp
	subq $88, %rsp
	movq %rdi, -8(%rbp)
	movq $15, %rdx
	movq %rdx, -16(%rbp)
	movq -8(%rbp),  %rax
	movq -16(%rbp),  %rdx
	cmp %rdx, %rax
	setg %al
	movzbq %al, %rax
	movq %rax, -48(%rbp)
	movq $0, %rax
	movq -48(%rbp),  %rbx
	cmp %rax, %rbx
	je .L7
	movq $1, %rax
	movq $-1, %rbx
	imul %rbx
	movq %rax, -56(%rbp)
	movq -56(%rbp), %rax
	jmp .L6
.L7:
	movq $0, %rdx
	movq %rdx, -24(%rbp)
	movq $1, %rdx
	movq %rdx, -32(%rbp)
.L8:
	movq -24(%rbp),  %rax
	movq -8(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -64(%rbp)
	movq $1, %rax
	movq -64(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L9
	movq -24(%rbp), %rax
	addq $1, %rax
	movq %rax, -72(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq -32(%rbp), %rax
	movq -24(%rbp), %rbx
	imul %rbx
	movq %rax, -80(%rbp)
	movq -80(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L8
.L9:
	movq -32(%rbp), %rax
	jmp .L6
	movq $0, %rax
	jmp .L6
	.L6:
	leave
	ret

.globl nthprime
.type nthprime, @function
nthprime:
	pushq %rbp
	movq %rsp, %rbp
	subq $136, %rsp
	movq %rdi, -8(%rbp)
	movq $0, %rdx
	movq %rdx, -16(%rbp)
	movq $2, %rdx
	movq %rdx, -24(%rbp)
	movq -8(%rbp), %rax
	addq $1, %rax
	movq %rax, -48(%rbp)
	movq -48(%rbp), %rdx
	movq %rdx, -8(%rbp)
.L11:
	movq -8(%rbp),  %rax
	movq $0,  %rdx
	cmp %rdx, %rax
	setg %al
	movzbq %al, %rax
	movq %rax, -56(%rbp)
	movq $1, %rax
	movq -56(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L12
	movq $0, %rdx
	movq %rdx, -32(%rbp)
	movq -16(%rbp), %rax
	addq $1, %rax
	movq %rax, -64(%rbp)
	movq -64(%rbp), %rdx
	movq %rdx, -16(%rbp)
.L13:
	movq -32(%rbp), %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -72(%rbp)
	movq -24(%rbp),  %rax
	movq -16(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -80(%rbp)
	movq -72(%rbp), %rax
	movq -80(%rbp), %rdx
	and  %rdx, %rax
	movq %rax, -88(%rbp)
	movq $1, %rax
	movq -88(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L14
	movq -16(%rbp), %rax
	movq -24(%rbp), %rbx
	cltd
	idiv %rbx
	movq %rdx, -96(%rbp)
	movq -96(%rbp),  %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -104(%rbp)
	movq $0, %rax
	movq -104(%rbp),  %rbx
	cmp %rax, %rbx
	je .L15
	movq $1, %rdx
	movq %rdx, -32(%rbp)
	jmp .L16
.L15:
	movq -24(%rbp), %rax
	addq $1, %rax
	movq %rax, -112(%rbp)
	movq -112(%rbp), %rdx
	movq %rdx, -24(%rbp)
.L16:
jmp .L13
.L14:
	movq $2, %rdx
	movq %rdx, -24(%rbp)
	movq -32(%rbp), %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -120(%rbp)
	movq $0, %rax
	movq -120(%rbp),  %rbx
	cmp %rax, %rbx
	je .L17
	movq -8(%rbp), %rax
	movq $1, %rbx
	subq %rbx, %rax
	movq %rax, -128(%rbp)
	movq -128(%rbp), %rdx
	movq %rdx, -8(%rbp)
.L17:
jmp .L11
.L12:
	movq -16(%rbp), %rax
	jmp .L10
	.L10:
	leave
	ret

.globl gcd
.type gcd, @function
gcd:
	pushq %rbp
	movq %rsp, %rbp
	subq $112, %rsp
	movq %rdi, -8(%rbp)
	movq %rsi, -16(%rbp)
	movq $1, %rdx
	movq %rdx, -24(%rbp)
	movq -24(%rbp), %rdx
	movq %rdx, -32(%rbp)
.L19:
	movq -8(%rbp), %rax
	addq -16(%rbp), %rax
	movq %rax, -48(%rbp)
	movq -24(%rbp),  %rax
	movq -48(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -56(%rbp)
	movq $1, %rax
	movq -56(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L20
	movq -8(%rbp), %rax
	movq -24(%rbp), %rbx
	cltd
	idiv %rbx
	movq %rdx, -64(%rbp)
	movq -64(%rbp),  %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -72(%rbp)
	movq -16(%rbp), %rax
	movq -24(%rbp), %rbx
	cltd
	idiv %rbx
	movq %rdx, -80(%rbp)
	movq -80(%rbp),  %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -88(%rbp)
	movq -72(%rbp), %rax
	movq -88(%rbp), %rdx
	and  %rdx, %rax
	movq %rax, -96(%rbp)
	movq $0, %rax
	movq -96(%rbp),  %rbx
	cmp %rax, %rbx
	je .L21
	movq -24(%rbp), %rdx
	movq %rdx, -32(%rbp)
.L21:
	movq -24(%rbp), %rax
	addq $1, %rax
	movq %rax, -104(%rbp)
	movq -104(%rbp), %rdx
	movq %rdx, -24(%rbp)
jmp .L19
.L20:
	movq -32(%rbp), %rax
	jmp .L18
	.L18:
	leave
	ret

.globl potencia
.type potencia, @function
potencia:
	pushq %rbp
	movq %rsp, %rbp
	subq $104, %rsp
	movq %rdi, -8(%rbp)
	movq %rsi, -16(%rbp)
	movq $0, %rdx
	movq %rdx, -40(%rbp)
	movq $1, %rdx
	movq %rdx, -32(%rbp)
	movq $1, %rdx
	movq %rdx, -24(%rbp)
.L23:
	movq -40(%rbp), %rax
	movq $0, %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -56(%rbp)
	movq $1, %rax
	movq -56(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L24
	movq -24(%rbp),  %rax
	movq -16(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -64(%rbp)
	movq -24(%rbp),  %rax
	movq -16(%rbp),  %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -72(%rbp)
	movq -64(%rbp), %rax
	movq -72(%rbp), %rbx
	or   %rax, %rbx
	movq %rbx, -80(%rbp)
	movq $0, %rax
	movq -80(%rbp),  %rbx
	cmp %rax, %rbx
	je .L25
	movq -32(%rbp), %rax
	movq -8(%rbp), %rbx
	imul %rbx
	movq %rax, -88(%rbp)
	movq -88(%rbp), %rdx
	movq %rdx, -32(%rbp)
	movq -24(%rbp), %rax
	addq $1, %rax
	movq %rax, -96(%rbp)
	movq -96(%rbp), %rdx
	movq %rdx, -24(%rbp)
	jmp .L26
.L25:
	movq $1, %rdx
	movq %rdx, -40(%rbp)
.L26:
jmp .L23
.L24:
	movq -32(%rbp), %rax
	jmp .L22
	.L22:
	leave
	ret

.globl potenciaI
.type potenciaI, @function
potenciaI:
	pushq %rbp
	movq %rsp, %rbp
	subq $88, %rsp
	movq %rdi, -8(%rbp)
	movq %rsi, -16(%rbp)
	movq $1, %rdx
	movq %rdx, -32(%rbp)
	movq $1, %rdx
	movq %rdx, -24(%rbp)
.L28:
	movq -24(%rbp),  %rax
	movq -16(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -48(%rbp)
	movq -24(%rbp),  %rax
	movq -16(%rbp),  %rbx
	cmp %rbx, %rax
	sete %al
	movzbq %al, %rax
	movq %rax, -56(%rbp)
	movq -48(%rbp), %rax
	movq -56(%rbp), %rbx
	or   %rax, %rbx
	movq %rbx, -64(%rbp)
	movq $1, %rax
	movq -64(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L29
	movq -32(%rbp), %rax
	movq -8(%rbp), %rbx
	imul %rbx
	movq %rax, -72(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -32(%rbp)
	movq -24(%rbp), %rax
	addq $1, %rax
	movq %rax, -80(%rbp)
	movq -80(%rbp), %rdx
	movq %rdx, -24(%rbp)
jmp .L28
.L29:
	movq -32(%rbp), %rax
	jmp .L27
	.L27:
	leave
	ret

.globl intTtoTbin
.type intTtoTbin, @function
intTtoTbin:
	pushq %rbp
	movq %rsp, %rbp
	subq $120, %rsp
	movq %rdi, -8(%rbp)
	movq $0, %rdx
	movq %rdx, -16(%rbp)
	movq $0, %rdx
	movq %rdx, -24(%rbp)
.L31:
	movq -8(%rbp),  %rax
	movq $0,  %rdx
	cmp %rdx, %rax
	setg %al
	movzbq %al, %rax
	movq %rax, -48(%rbp)
	movq $1, %rax
	movq -48(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L32
	movq -8(%rbp), %rax
	movq 	$2, %rbx
	cltd
	idiv %rbx
	movq %rdx, -56(%rbp)
	movq -56(%rbp), %rdx
	movq %rdx, -32(%rbp)
	movq -8(%rbp), %rax
	movq 	$2, %rbx
	cltd
	idiv %rbx
	movq %rax,-64(%rbp)
	movq -64(%rbp), %rdx
	movq %rdx, -8(%rbp)
	movq $10, %rdx
	movq %rdx, -72(%rbp)
	movq -24(%rbp), %rdx
	movq %rdx, -80(%rbp)
	movq -72(%rbp), %rdi
	movq -80(%rbp), %rsi
	call potenciaI
	movq %rax, -88(%rbp)
	movq -88(%rbp), %rax
	movq -32(%rbp), %rbx
	imul %rbx
	movq %rax, -96(%rbp)
	movq -16(%rbp), %rax
	addq -96(%rbp), %rax
	movq %rax, -104(%rbp)
	movq -104(%rbp), %rdx
	movq %rdx, -16(%rbp)
	movq -24(%rbp), %rax
	addq $1, %rax
	movq %rax, -112(%rbp)
	movq -112(%rbp), %rdx
	movq %rdx, -24(%rbp)
jmp .L31
.L32:
	movq -16(%rbp), %rax
	jmp .L30
	.L30:
	leave
	ret

.globl test
.type test, @function
test:
	pushq %rbp
	movq %rsp, %rbp
	subq $128, %rsp
	movq $2, %rdx
	movq %rdx, -8(%rbp)
	movq $3, %rdx
	movq %rdx, -32(%rbp)
	movq -32(%rbp), %rdi
	call factorial
	movq %rax, -40(%rbp)
	movq $4, %rdx
	movq %rdx, -48(%rbp)
	movq -48(%rbp), %rdi
	call factorial
	movq %rax, -56(%rbp)
	movq -40(%rbp), %rdi
	movq -56(%rbp), %rsi
	call gcd
	movq %rax, -64(%rbp)
	movq -64(%rbp), %rdx
	movq %rdx, -8(%rbp)
	movq $3, %rdx
	movq %rdx, -72(%rbp)
	movq -72(%rbp), %rdi
	call factorial
	movq %rax, -80(%rbp)
	movq -8(%rbp), %rdx
	movq %rdx, -88(%rbp)
	movq -80(%rbp), %rdi
	movq -88(%rbp), %rsi
	call gcd
	movq %rax, -96(%rbp)
	movq -96(%rbp), %rdi
	call nthprime
	movq %rax, -104(%rbp)
	movq -104(%rbp), %rdx
	movq %rdx, -8(%rbp)
	movq -8(%rbp), %rdx
	movq %rdx, -112(%rbp)
	movq -112(%rbp), %rdi
	call printTint
	movq %rax, -120(%rbp)
	movq -120(%rbp), %rdx
	movq %rdx, -16(%rbp)
	movq -8(%rbp), %rax
	jmp .L33
	.L33:
	leave
	ret

.globl test1
.type test1, @function
test1:
	pushq %rbp
	movq %rsp, %rbp
	subq $80, %rsp
	movq $1, %rdx
	movq %rdx, -8(%rbp)
	movq $4, %rdx
	movq %rdx, -24(%rbp)
	movq -24(%rbp), %rdi
	call nthprime
	movq %rax, -32(%rbp)
	movq -8(%rbp), %rax
	addq $1, %rax
	movq %rax, -40(%rbp)
	movq -32(%rbp), %rdi
	movq -40(%rbp), %rsi
	call potencia
	movq %rax, -48(%rbp)
	movq -48(%rbp), %rdi
	call printTint
	movq %rax, -56(%rbp)
	movq -56(%rbp), %rdx
	movq %rdx, -8(%rbp)
	call test
	movq %rax, -64(%rbp)
	movq -64(%rbp), %rdi
	call printTint
	movq %rax, -72(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -8(%rbp)
	movq -8(%rbp), %rax
	jmp .L34
	.L34:
	leave
	ret

.globl main
.type main, @function
main:
	pushq %rbp
	movq %rsp, %rbp
	subq $976, %rsp
	movq $0, %rdx
	movq %rdx, -32(%rbp)
	call initTinput
	movq %rax, -16(%rbp)
	movq -16(%rbp), %rdx
	movq %rdx, -48(%rbp)
	movq $2, %rdx
	movq %rdx, -24(%rbp)
	movq -24(%rbp), %rdi
	call printString
	movq %rax, -32(%rbp)
	movq -32(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -40(%rbp)
	movq -40(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L35:
	movq -32(%rbp),  %rax
	movq -24(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -48(%rbp)
	movq $1, %rax
	movq -48(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L36
	call getTint
	movq %rax, -56(%rbp)
	movq -56(%rbp), %rdx
	movq %rdx, -56(%rbp)
	movq -56(%rbp), %rdx
	movq %rdx, -64(%rbp)
	movq -64(%rbp), %rdi
	call printStringFactorialValor
	movq %rax, -72(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -56(%rbp), %rdx
	movq %rdx, -80(%rbp)
	movq -80(%rbp), %rdi
	call factorial
	movq %rax, -88(%rbp)
	movq -88(%rbp), %rdx
	movq %rdx, -56(%rbp)
	movq -56(%rbp), %rdx
	movq %rdx, -96(%rbp)
	movq -96(%rbp), %rdi
	call printTint
	movq %rax, -104(%rbp)
	movq -104(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -112(%rbp)
	movq -112(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L35
.L36:
	movq $1, %rdx
	movq %rdx, -120(%rbp)
	movq -120(%rbp), %rdi
	call printString
	movq %rax, -128(%rbp)
	movq -128(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $3, %rdx
	movq %rdx, -136(%rbp)
	movq -136(%rbp), %rdi
	call printString
	movq %rax, -144(%rbp)
	movq -144(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -152(%rbp)
	movq -152(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L37:
	movq -32(%rbp),  %rax
	movq -24(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -160(%rbp)
	movq $1, %rax
	movq -160(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L38
	call getTint
	movq %rax, -168(%rbp)
	movq -168(%rbp), %rdx
	movq %rdx, -64(%rbp)
	call getTint
	movq %rax, -176(%rbp)
	movq -176(%rbp), %rdx
	movq %rdx, -72(%rbp)
	movq -64(%rbp), %rdx
	movq %rdx, -184(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -192(%rbp)
	movq -184(%rbp), %rdi
	movq -192(%rbp), %rsi
	call printStringPotenciaIValor
	movq %rax, -200(%rbp)
	movq -200(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -64(%rbp), %rdx
	movq %rdx, -208(%rbp)
	movq -72(%rbp), %rdx
	movq %rdx, -216(%rbp)
	movq -208(%rbp), %rdi
	movq -216(%rbp), %rsi
	call potenciaI
	movq %rax, -224(%rbp)
	movq -224(%rbp), %rdx
	movq %rdx, -80(%rbp)
	movq -80(%rbp), %rdx
	movq %rdx, -232(%rbp)
	movq -232(%rbp), %rdi
	call printTint
	movq %rax, -240(%rbp)
	movq -240(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -248(%rbp)
	movq -248(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L37
.L38:
	movq $1, %rdx
	movq %rdx, -256(%rbp)
	movq -256(%rbp), %rdi
	call printString
	movq %rax, -264(%rbp)
	movq -264(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $4, %rdx
	movq %rdx, -272(%rbp)
	movq -272(%rbp), %rdi
	call printString
	movq %rax, -280(%rbp)
	movq -280(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -288(%rbp)
	movq -288(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L39:
	movq -32(%rbp),  %rax
	movq -24(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -296(%rbp)
	movq $1, %rax
	movq -296(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L40
	call getTint
	movq %rax, -304(%rbp)
	movq -304(%rbp), %rdx
	movq %rdx, -88(%rbp)
	call getTint
	movq %rax, -312(%rbp)
	movq -312(%rbp), %rdx
	movq %rdx, -96(%rbp)
	movq -88(%rbp), %rdx
	movq %rdx, -320(%rbp)
	movq -96(%rbp), %rdx
	movq %rdx, -328(%rbp)
	movq -320(%rbp), %rdi
	movq -328(%rbp), %rsi
	call printStringPotenciaValor
	movq %rax, -336(%rbp)
	movq -336(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -88(%rbp), %rdx
	movq %rdx, -344(%rbp)
	movq -96(%rbp), %rdx
	movq %rdx, -352(%rbp)
	movq -344(%rbp), %rdi
	movq -352(%rbp), %rsi
	call potencia
	movq %rax, -360(%rbp)
	movq -360(%rbp), %rdx
	movq %rdx, -104(%rbp)
	movq -104(%rbp), %rdx
	movq %rdx, -368(%rbp)
	movq -368(%rbp), %rdi
	call printTint
	movq %rax, -376(%rbp)
	movq -376(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -384(%rbp)
	movq -384(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L39
.L40:
	movq $1, %rdx
	movq %rdx, -392(%rbp)
	movq -392(%rbp), %rdi
	call printString
	movq %rax, -400(%rbp)
	movq -400(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $5, %rdx
	movq %rdx, -408(%rbp)
	movq -408(%rbp), %rdi
	call printString
	movq %rax, -416(%rbp)
	movq -416(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -424(%rbp)
	movq -424(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L41:
	movq -32(%rbp),  %rax
	movq -24(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -432(%rbp)
	movq $1, %rax
	movq -432(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L42
	call getTint
	movq %rax, -440(%rbp)
	movq -440(%rbp), %rdx
	movq %rdx, -112(%rbp)
	movq -112(%rbp), %rdx
	movq %rdx, -448(%rbp)
	movq -448(%rbp), %rdi
	call printStringNthprimeValor
	movq %rax, -456(%rbp)
	movq -456(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -112(%rbp), %rdx
	movq %rdx, -464(%rbp)
	movq -464(%rbp), %rdi
	call nthprime
	movq %rax, -472(%rbp)
	movq -472(%rbp), %rdx
	movq %rdx, -112(%rbp)
	movq -112(%rbp), %rdx
	movq %rdx, -480(%rbp)
	movq -480(%rbp), %rdi
	call printTint
	movq %rax, -488(%rbp)
	movq -488(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -496(%rbp)
	movq -496(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L41
.L42:
	movq $1, %rdx
	movq %rdx, -504(%rbp)
	movq -504(%rbp), %rdi
	call printString
	movq %rax, -512(%rbp)
	movq -512(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $7, %rdx
	movq %rdx, -520(%rbp)
	movq -520(%rbp), %rdi
	call printString
	movq %rax, -528(%rbp)
	movq -528(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -536(%rbp)
	movq -536(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L43:
	movq -32(%rbp),  %rax
	movq -24(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -544(%rbp)
	movq $1, %rax
	movq -544(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L44
	call getTint
	movq %rax, -552(%rbp)
	movq -552(%rbp), %rdx
	movq %rdx, -120(%rbp)
	movq -120(%rbp), %rdx
	movq %rdx, -560(%rbp)
	movq -560(%rbp), %rdi
	call printStringInt2BinValor
	movq %rax, -568(%rbp)
	movq -568(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -120(%rbp), %rdx
	movq %rdx, -576(%rbp)
	movq -576(%rbp), %rdi
	call intTtoTbin
	movq %rax, -584(%rbp)
	movq -584(%rbp), %rdx
	movq %rdx, -120(%rbp)
	movq -120(%rbp), %rdx
	movq %rdx, -592(%rbp)
	movq -592(%rbp), %rdi
	call printTint
	movq %rax, -600(%rbp)
	movq -600(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -608(%rbp)
	movq -608(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L43
.L44:
	movq $1, %rdx
	movq %rdx, -616(%rbp)
	movq -616(%rbp), %rdi
	call printString
	movq %rax, -624(%rbp)
	movq -624(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $8, %rdx
	movq %rdx, -632(%rbp)
	movq -632(%rbp), %rdi
	call printString
	movq %rax, -640(%rbp)
	movq -640(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -648(%rbp)
	movq -648(%rbp), %rdx
	movq %rdx, -48(%rbp)
	call getTint
	movq %rax, -656(%rbp)
	movq -656(%rbp), %rdx
	movq %rdx, -48(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L45:
	movq -32(%rbp),  %rax
	movq -48(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -664(%rbp)
	movq $1, %rax
	movq -664(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L46
	call getTint
	movq %rax, -672(%rbp)
	movq -672(%rbp), %rdx
	movq %rdx, -128(%rbp)
	call getTint
	movq %rax, -680(%rbp)
	movq -680(%rbp), %rdx
	movq %rdx, -136(%rbp)
	movq -128(%rbp), %rdx
	movq %rdx, -688(%rbp)
	movq -136(%rbp), %rdx
	movq %rdx, -696(%rbp)
	movq -688(%rbp), %rdi
	movq -696(%rbp), %rsi
	call printStringGCDValor
	movq %rax, -704(%rbp)
	movq -704(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -128(%rbp), %rdx
	movq %rdx, -712(%rbp)
	movq -136(%rbp), %rdx
	movq %rdx, -720(%rbp)
	movq -712(%rbp), %rdi
	movq -720(%rbp), %rsi
	call gcd
	movq %rax, -728(%rbp)
	movq -728(%rbp), %rdx
	movq %rdx, -144(%rbp)
	movq -144(%rbp), %rdx
	movq %rdx, -736(%rbp)
	movq -736(%rbp), %rdi
	call printTint
	movq %rax, -744(%rbp)
	movq -744(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -752(%rbp)
	movq -752(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L45
.L46:
	movq $1, %rdx
	movq %rdx, -760(%rbp)
	movq -760(%rbp), %rdi
	call printString
	movq %rax, -768(%rbp)
	movq -768(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $6, %rdx
	movq %rdx, -776(%rbp)
	movq -776(%rbp), %rdi
	call printString
	movq %rax, -784(%rbp)
	movq -784(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call getTint
	movq %rax, -792(%rbp)
	movq -792(%rbp), %rdx
	movq %rdx, -24(%rbp)
	movq $0, %rdx
	movq %rdx, -32(%rbp)
.L47:
	movq -32(%rbp),  %rax
	movq -24(%rbp),  %rdx
	cmp %rdx, %rax
	setl %al
	movzbq %al, %rax
	movq %rax, -800(%rbp)
	movq $1, %rax
	movq -800(%rbp),  %rbx
	cmp %rax, %rbx
	jne .L48
	call getTint
	movq %rax, -808(%rbp)
	movq -808(%rbp), %rdx
	movq %rdx, -152(%rbp)
	movq -152(%rbp), %rdx
	movq %rdx, -816(%rbp)
	movq -816(%rbp), %rdi
	call printStringFibonacciValor
	movq %rax, -824(%rbp)
	movq -824(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -152(%rbp), %rdx
	movq %rdx, -832(%rbp)
	movq -832(%rbp), %rdi
	call fibonacci
	movq %rax, -840(%rbp)
	movq -840(%rbp), %rdx
	movq %rdx, -160(%rbp)
	movq -160(%rbp), %rdx
	movq %rdx, -848(%rbp)
	movq -848(%rbp), %rdi
	call printTint
	movq %rax, -856(%rbp)
	movq -856(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq -32(%rbp), %rax
	addq $1, %rax
	movq %rax, -864(%rbp)
	movq -864(%rbp), %rdx
	movq %rdx, -32(%rbp)
jmp .L47
.L48:
	movq $1, %rdx
	movq %rdx, -872(%rbp)
	movq -872(%rbp), %rdi
	call printString
	movq %rax, -880(%rbp)
	movq -880(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $9, %rdx
	movq %rdx, -888(%rbp)
	movq -888(%rbp), %rdi
	call printString
	movq %rax, -896(%rbp)
	movq -896(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call test
	movq %rax, -904(%rbp)
	movq -904(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $1, %rdx
	movq %rdx, -912(%rbp)
	movq -912(%rbp), %rdi
	call printString
	movq %rax, -920(%rbp)
	movq -920(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $10, %rdx
	movq %rdx, -928(%rbp)
	movq -928(%rbp), %rdi
	call printString
	movq %rax, -936(%rbp)
	movq -936(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call test1
	movq %rax, -944(%rbp)
	movq -944(%rbp), %rdx
	movq %rdx, -40(%rbp)
	movq $1, %rdx
	movq %rdx, -952(%rbp)
	movq -952(%rbp), %rdi
	call printString
	movq %rax, -960(%rbp)
	movq -960(%rbp), %rdx
	movq %rdx, -40(%rbp)
	call closeTinput
	movq %rax, -968(%rbp)
	movq -968(%rbp), %rdx
	movq %rdx, -48(%rbp)
	leave
	ret

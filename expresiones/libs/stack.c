#ifndef _STACK_C
#define _STACK_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stack.h"
#include "param.h"
#include "table.h"

static int levelA = 0;

stack* getLevel(stack* st, int level){
	stack* aux = st;
	while (aux->level != level){
		aux->sig;
	}
	return aux;
}

stack* insertLevel(stack* st){
	stack* newLevel = malloc(sizeof(stack));
	newLevel->sig = st;
	levelA = levelA + 1;
	newLevel->level = levelA;
	return newLevel;
}

stack* deleteLevel(stack* st){
	levelA = levelA - 1;
	return st->sig;
}

table* searchInStack(stack* stk, info* v, int bool){ //modificar
	stack* aux = stk;
	while (aux != NULL){
		table* a = search_in_list(aux->simb, v, 0);
		if (a != NULL)
			return a;
		aux = aux->sig;
	}
	if (bool){
		if(aux == NULL){
			printf("ERROR --> linea %d : Variable %s no definida.\n", v->line, v->name);
			exit(1);
		}
	}
	return NULL;
}

stack* insertInLevels(stack* stk, info* v){ 
	stk->simb = insert_in_list(stk->simb, v);	
	return stk;
}

stack* insertParamInLevel(stack* s, lpar* p){
	lpar* aux = p;
	stack* saux = s; 
	info* par;
	while (aux != NULL){
		par = load_info_name(aux->name, 8);
		par->ty = aux->ty;
		par->offset = get_offset();
		saux = insertInLevels(saux, par);
		aux = aux->sig;
	}
	return saux;
}

#endif

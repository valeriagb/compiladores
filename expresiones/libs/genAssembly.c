#ifndef _GENASSEMBLY_C
#define _GENASSEMBLY_C

#include <stdio.h>
#include <stdlib.h>
#include "instruc.h"

instruc* asiGlob;
instruc* asigEnd;

int generator_init(FILE *arch, instruc* list){
	fprintf(arch, ".globl %s\n", list->op1->name);
	fprintf(arch, ".type %s, @function\n", list->op1->name);
	fprintf(arch, "%s:\n", list->op1->name);
	fprintf(arch, "\tpushq %%rbp\n");	
	fprintf(arch, "\tmovq %%rsp, %%rbp\n"); 
	int actualOffs = list->op3->val * (-1);
	fprintf(arch, "\tsubq $%d, %%rsp\n", actualOffs); 
	return 0;
}

int generator_end(FILE* arch){
	fprintf(arch, "\tleave\n");
	fprintf(arch, "\tret\n");
	return 0;
}

char* createNameJmp(int label){
	char *x=malloc(sizeof(char)*5);
	sprintf(x, ".L%d", label);
	return x;
}

int gen_load_param(FILE* arch, instruc* list){
	switch (list->op3->val){
		case 1:
			fprintf(arch, "\tmovq %d(%%rbp), %%rdi\n", list->op1->offset);
			break;
		case 2:
			fprintf(arch, "\tmovq %d(%%rbp), %%rsi\n", list->op1->offset);
			break;
		case 3:
			fprintf(arch, "\tmovq %d(%%rbp), %%rdx\n", list->op1->offset);
			break;
		case 4:
			fprintf(arch, "\tmovq %d(%%rbp), %%rcx\n", list->op1->offset);
			break;
		case 5:
			fprintf(arch, "\tmovq %d(%%rbp), %%r8\n", list->op1->offset);
			break;
		case 6:
			fprintf(arch, "\tmovq %d(%%rbp), %%r9\n", list->op1->offset);
			break;
	}
	return 0;
}

int gen_save_param(FILE* arch, instruc* list){
	switch (list->op3->val){
		case 1:
			fprintf(arch, "\tmovq %%rdi, -8(%%rbp)\n");
			break;
		case 2:
			fprintf(arch, "\tmovq %%rsi, -16(%%rbp)\n");
			break;
		case 3:
			fprintf(arch, "\tmovq %%rdx, -24(%%rbp)\n");
			break;
		case 4:
			fprintf(arch, "\tmovq %%rcx, -32(%%rbp)\n");
			break;
		case 5:
			fprintf(arch, "\tmovq %%r8, -40(%%rbp)\n");
			break;
		case 6:
			fprintf(arch, "\tmovq %%r9, -48(%%rbp)\n");
			break;
	}
	return 0;
}

int assemblyOperations(FILE* archive, instruc* list){
	if (list->i == SUMA){
		fprintf(archive, "\tmovq "); 
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op1->val);
		}
		fprintf(archive, "%%rax\n");
		fprintf(archive, "\taddq ");
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op2->val);
		}
		fprintf(archive, "%%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == RESTA){
		fprintf(archive, "\tmovq "); 
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op1->val);
		}
		fprintf(archive, "%%rax\n");
		fprintf(archive, "\tmovq "); 
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op2->val);
		}
		fprintf(archive, "%%rbx\n");
		fprintf(archive, "\tsubq %%rbx, %%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == MULT){
		fprintf(archive, "\tmovq "); 
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op1->val);
		}
		fprintf(archive, "%%rax\n");
		fprintf(archive, "\tmovq "); 
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op2->val);
		}
		fprintf(archive, "%%rbx\n");
		fprintf(archive, "\timul %%rbx\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip)\n", list->op3->name);
			}else{
				fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
			}
	}
	if (list->i == DIV || list->i == MOD){
		fprintf(archive, "\tmovq "); 
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}
		}
		else{
			fprintf(archive, "$%d, ", list->op1->val);
		}
		fprintf(archive, "%%rax\n");
		fprintf(archive, "\tmovq "); 
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}
		}
		else{
			fprintf(archive, "\t$%d, ", list->op2->val);
		}
		fprintf(archive, "%%rbx\n");
		fprintf(archive, "\tcltd\n");
		fprintf(archive, "\tidiv %%rbx\n");
		if(list->i == DIV){
			fprintf(archive, "\tmovq %%rax,");
		}else{
			fprintf(archive, "\tmovq %%rdx, ");
		}
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == ASIG){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), %%rdx\n", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), %%rdx\n", list->op1->offset);
			}		
		}
		else{
			fprintf(archive, "$%d, %%rdx\n", list->op1->val);
		}
		fprintf(archive, "\tmovq %%rdx, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == PRINT){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), %%rdi\n", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), %%rdi\n", list->op1->offset);
			}
		}
		else{
			fprintf(archive, "$%d, %%rdi\n", list->op1->val);
		}
		fprintf(archive, "\tcall printi\n");
	}
	if (list->i == ORR){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}	
		}
		else {
			fprintf(archive, "$%d, ", list->op1->val);
		}
		fprintf(archive, "%%rax\n");
		fprintf(archive, "\tmovq ");
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}	
		}
		else {
			fprintf(archive, "$%d, ", list->op2->val);
		}
		fprintf(archive, "%%rbx\n");
		fprintf(archive, "\tor   %%rax, %%rbx\n");
		fprintf(archive, "\tmovq %%rbx, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}

	if (list->i == ANDD){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), %%rax\n", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), %%rax\n", list->op1->offset);
			}	
		}
		else {
			fprintf(archive, "$%d, %%rax\n", list->op1->val);
		}
		fprintf(archive, "\tmovq ");
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), %%rdx\n", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), %%rdx\n", list->op2->offset);
			}
		}
		else {
			fprintf(archive, "$%d, %%rdx\n", list->op2->val);
		}
		fprintf(archive, "\tand  %%rdx, %%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == NEG){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), %%rax\n", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), %%rax\n", list->op1->offset);
			}			
		}
		fprintf(archive, "\tmovq $0, %%rbx\n");
		fprintf(archive, "\tcmp %%rbx, %%rax\n");
		fprintf(archive, "\tsete %%al\n");
		fprintf(archive, "\tmovzbq %%al, %%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}

	}
	if (list->i == MAYOR){
		fprintf(archive, "\tmovq ");		
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}		
		}	
		else{
			fprintf(archive, "$%d, ", list->op1->val);
		}
		fprintf(archive, " %%rax\n");
		fprintf(archive, "\tmovq ");
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}			
		}
		else{
			fprintf(archive, "$%d, ", list->op2->val);
		}
		fprintf(archive, " %%rdx\n");
		fprintf(archive, "\tcmp %%rdx, %%rax\n");
		fprintf(archive, "\tsetg %%al\n");
		fprintf(archive, "\tmovzbq %%al, %%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == MENOR){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}	
		}
		else{
			fprintf(archive, "$%d,", list->op1->val);
		}
		fprintf(archive, " %%rax\n");
		fprintf(archive, "\tmovq ");
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}		
		}
		else{
			fprintf(archive, "$%d, ", list->op2->val);
		}
		fprintf(archive, " %%rdx\n");
		fprintf(archive, "\tcmp %%rdx, %%rax\n");
		fprintf(archive, "\tsetl %%al\n");
		fprintf(archive, "\tmovzbq %%al, %%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	if (list->i == EQ){
		fprintf(archive, "\tmovq ");
		if (list->op1->name != NULL){
			if (list->op1->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op1->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}
		}
		else{
			fprintf(archive, "$%d,", list->op1->val);
		}
		fprintf(archive, " %%rax\n");
		fprintf(archive, "\tmovq ");
		if (list->op2->name != NULL){
			if (list->op2->tinfo == TGLOBAL){
				fprintf(archive, "%s(%%rip), ", list->op2->name);
			}else{
				fprintf(archive, "%d(%%rbp), ", list->op2->offset);
			}	
		}
		else{
			fprintf(archive, "$%d,", list->op2->val);
		}
		fprintf(archive, " %%rbx\n");
		fprintf(archive, "\tcmp %%rbx, %%rax\n");
		fprintf(archive, "\tsete %%al\n");
		fprintf(archive, "\tmovzbq %%al, %%rax\n");
		fprintf(archive, "\tmovq %%rax, ");
		if (list->op3->tinfo == TGLOBAL){
			fprintf(archive, "%s(%%rip)\n", list->op3->name);
		}else{
			fprintf(archive, "%d(%%rbp)\n", list->op3->offset);
		}
	}
	return 0;
}


//Genera archivo .s con la lista del codigo de 3 instrucciones
int gen_file(instruc* list){
	FILE *archive;
    archive = fopen("./a.s","w");
	while(list != NULL){
		if (list->i == MAINN){
			fprintf(archive, "\n");
			fprintf(archive, ".globl main\n");
			fprintf(archive, ".type main, @function\n");
			fprintf(archive, "main:\n");
			fprintf(archive, "\tpushq %%rbp\n");	
			fprintf(archive, "\tmovq %%rsp, %%rbp\n"); 
			int actualOffs = list->op3->val * (-1);
			fprintf(archive, "\tsubq $%d, %%rsp\n", actualOffs);
			if (asiGlob != NULL){
				asiGlob = asiGlob->sig;
				printf("ssssssssssssssssssssss\n");
				while (asiGlob->i != ENDGLOBAL){
					assemblyOperations(archive, asiGlob);
					asiGlob = asiGlob->sig;
				} 
			}
		}
		if (list->i == GLOBAL){
			list = list->sig;
			asiGlob = malloc(sizeof(instruc));
			asigEnd = asiGlob;
			instruc* aux = list;
			while (list->i != ENDGLOBAL){
				if ((list->i == DECL) || (list->i == ASIG)){
					fprintf(archive, "\t.comm	%s,8,8\n", list->op3->name);
				}
				if (list->i != DECL ){
					asigEnd->sig = aux;
					asigEnd = asigEnd->sig;
				}
				aux = list->sig;
				list = list->sig;				
			}
//			asigEnd->sig = malloc(sizeof(instruc));
		}

		if (list->i == FUN){
			fprintf(archive, "\n");
			generator_init(archive, list);
		}
		if (list->i == LOADPARAM){
			gen_load_param(archive, list);
		}
		if (list->i == SAVEP){
			gen_save_param(archive, list);
		}
		if (list->i == CALL){
			fprintf(archive, "\tcall %s\n", list->op1->name);
			fprintf(archive, "\tmovq %%rax, %d(%%rbp)\n", list->op3->offset);

		}
		if (list->i == ENDFUN){
			fprintf(archive, "\t%s:\n", createNameJmp(list->op3->val));
			generator_end(archive);

		}
		if (list->i == RETURNN){
			fprintf(archive, "\tmovq "); 
			if (list->op1->name != NULL){
				if (list->op1->tinfo == TGLOBAL){
					fprintf(archive, "%s(%%rip), ", list->op1->name);
				}
				fprintf(archive, "%d(%%rbp), ", list->op1->offset);
			}
			else{
				fprintf(archive, "$%d, ", list->op1->val);
			}
			fprintf(archive, "%%rax\n");
			fprintf(archive, "\tjmp %s\n", createNameJmp(list->op3->val));
		}
		if (list->i == SUMA){
			assemblyOperations(archive, list);
		}
		if (list->i == RESTA){
			assemblyOperations(archive, list);
		}
		if (list->i == MULT){
			assemblyOperations(archive, list);
		}
		if (list->i == DIV || list->i == MOD){
			assemblyOperations(archive, list);
		}
		if (list->i == ASIG){
			assemblyOperations(archive, list);
		}
		if (list->i == PRINT){
			assemblyOperations(archive, list);
		}
		if (list->i == ORR){
			assemblyOperations(archive, list);
		}	
		if (list->i == ANDD){
			assemblyOperations(archive, list);
		}
		if (list->i == NEG){
			assemblyOperations(archive, list);
		}
		if (list->i == MAYOR){
			assemblyOperations(archive, list);
		}
		if (list->i == MENOR){
			assemblyOperations(archive, list);
		}
		if (list->i == EQ){
			assemblyOperations(archive, list);
		}
		if (list->i == IFF){ //SALTO POR FALSE
			fprintf(archive, "\tmovq $0, %%rax\n");
			fprintf(archive, "\tmovq ");
			if (list->op1->name != NULL){
				if (list->op1->tinfo == TGLOBAL){
					fprintf(archive, "%s(%%rip), ", list->op1->name);
				}else{
					fprintf(archive, "%d(%%rbp), ", list->op1->offset);
				}
			}
			else{
				fprintf(archive, "$%d,", list->op1->val);
			}
			fprintf(archive, " %%rbx\n");
			fprintf(archive, "\tcmp %%rax, %%rbx\n");
			char* jmp = createNameJmp(list->op3->val);
			fprintf(archive, "\tje %s\n", jmp);

		}
		if (list->i == GOTO){
			fprintf(archive, "\tjmp %s\n", createNameJmp(list->op1->val));
		}
		if (list->i == ENDTHEN){
			if (list->sig != NULL && list->sig->i != ELSSE){
				fprintf(archive, "%s:\n", createNameJmp(list->op1->val));
			}
		}
		if (list->i == ELSSE){
			char* jmp = createNameJmp(list->op1->val);
			fprintf(archive, "%s:\n", jmp);
		}
		if (list->i == ENDELSE){
			char* jmp = createNameJmp(list->op1->val);
			fprintf(archive, "%s:\n", jmp);
		}
		if (list->i == LABEL){
			fprintf(archive, "%s:\n", createNameJmp(list->op3->val));
		}
		if (list->i == WHHILE){
			fprintf(archive, "\tmovq $1, %%rax\n");
			fprintf(archive, "\tmovq ");
			if (list->op1->name != NULL){
				if (list->op1->tinfo == TGLOBAL){
					fprintf(archive, "%s(%%rip), ", list->op1->name);
				}else{
					fprintf(archive, "%d(%%rbp), ", list->op1->offset);
				}
			}
			else{
				fprintf(archive, "$%d,", list->op1->val);
			}
			fprintf(archive, " %%rbx\n");
			fprintf(archive, "\tcmp %%rax, %%rbx\n");
			fprintf(archive, "\tjne %s\n", createNameJmp(list->op3->val));
		}
		if (list->i == ENDWHILE) {
			fprintf(archive, "jmp %s\n", createNameJmp(list->op2->val));
			fprintf(archive, "%s:\n", createNameJmp(list->op3->val));
		}
		list = list->sig;
	}

	generator_end(archive);
	fclose(archive);
	
	return 0;
}



#endif
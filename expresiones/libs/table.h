#ifndef _TABLE_H
#define _TABLE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "structures.h"

int reset_offset();

int get_offset();

int set_offset(int n);

struct info* load_info_val(int v, int line);

struct info* load_info_name(char* name, int line);

struct info* update_val(info* i, int v);

struct table* search_in_list(table* head, info* id, int bool);

struct table* insert_in_list(table* head, info* id);

int printTable(table* head);

#endif
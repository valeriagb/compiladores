#ifndef _PARAM_H
#define _PARAM_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "structures.h"

lpar* load_param(lpar* p, char* name);

int printParam(lpar* h);

#endif
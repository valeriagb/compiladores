#ifndef _TREE_C
#define _TREE_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tree.h"
#include "param.h"

node* load_node(node* n, node* sl, node* sr, info* d, char* l){
	n->hi = sl;
	n->hd = sr;
	n->data = d;
	n->label = l;
	return n;
}

int checkParamForm(node* n){
	lpar* aux = n->data->p;
	int p = 0;
	while (aux != NULL){
		p = p+1;
		aux = aux->sig;
	}
	if (p > 6){
		printf("Error semantico: Cantidad de parametros no soportada -> Linea: %d \n", n->data->line);
		exit(1);
	}
	return 0;
}

int checkParam(node* n){
	lpar* formp = n->data->p; // list param formales
	node* actualp = n->hd;
	while (formp != NULL){
		if (actualp != NULL){
			if (formp->ty != actualp->hi->data->ty){
				printf("Error semantico: Tipo de parametros incompatibles -> Linea: %d \n", n->data->line);
				exit(1);
			}
			detect_error(actualp->hi);
			formp = formp->sig;
			actualp = actualp->hd;
		} else{
			printf("Error semantico: Cantidad de parametros incompatibles -> Linea: %d \n", n->data->line);
			exit(1);
		}
	}
	if (formp == NULL && actualp != NULL){
			printf("Error semantico: Cantidad de paramentros excedida -> Linea: %d \n", n->data->line);
			exit(1);
	}
	return 0;
}

int checkReturn(node* n){
	node* b = n->hd;
	while (b->hd != NULL){
		if (b->hi != NULL){
			if (strcmp(b->hi->label, "RETURN") == 0){
				if(b->hi->data->ty != n->data->ty){
					printf("Error semantico: retorno no compatible -> Linea: %d\n", b->hi->data->line);
					exit(1);
				}
			}
		}
		if (strcmp(b->hd->label, "RETURN") == 0){
			if(b->hd->hd->data->ty != n->data->ty){
				printf("Error semantico: retorno no compatible -> Linea: %d\n", b->hd->hd->data->line);
				exit(1);
			}
		}
		b = b->hd;
	}
	if (b->hi != NULL){
		if (strcmp(b->hi->label, "RETURN") == 0)
			if(b->hi->data->ty != n->data->ty){
				printf("Error semantico: retorno no compatible -> Linea: %d\n", b->hi->data->line);
				exit(1);
			}
	}
	return 0;
}

node* searchReturn(node* n){
	node* nx = n->hd->hd;
	while (nx->hi != NULL && nx->hd != NULL){
		nx = nx->hd;
	}
	if (nx->hi != NULL){
		if (strcmp(nx->hi->label, "RETURN") == 0){
			return nx->hi;
		}else
			return NULL;
	}else{
		if (strcmp(nx->label, "RETURN") == 0){
			return nx;
		}else
			return NULL;
	}
}

int detect_error(node* n){
	if((strcmp(n->label, "CONS") == 0) || (strcmp(n->label, "BOOL") == 0)){ //constante
		return n->data->val;
	}
	if(strcmp(n->label,"MAIN") == 0 || strcmp(n->label,"BLOCK") == 0 || strcmp(n->label,"BODYIF") == 0 || strcmp(n->label,"NEXT") == 0 || strcmp(n->label,"GLOBAL") == 0 || strcmp(n->label,"FUNCTIONS") == 0 || strcmp(n->label,"DECLS") == 0){ //siguiente instruccion
		if(n->hi != NULL)
			detect_error(n->hi);
		if(n->hd != NULL)
			detect_error(n->hd);
		return 0;
	}

	if(strcmp(n->label,"BLOCK") == 0){ //siguiente instruccion
		if(n->hd != NULL)
			detect_error(n->hd);
		return 0;
	}

	if(strcmp(n->label,"ASIG") == 0){ //asig
		
		if (n->hi->data->ty != n->hd->data->ty){
			printf("Error semantico: el tipo de los elementos no es compatible. ASIG -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
	}

	if (strcmp(n->label,"SUMA") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion SUMA -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"RESTA") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion RESTA -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"MULT") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion MULT -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"DIV") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion DIV -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);

	}
	if (strcmp(n->label,"MOD") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion MOD -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);

	}
	if (strcmp(n->label,"MAYOR") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion MAYOR -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"MENOR") == 0){
		if (n->hi->data->ty != TINT || n->hd->data->ty != TINT){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion MENOR -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"IGUAL") == 0){
		if (n->hi->data->ty != n->hd->data->ty){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion EQUALS -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"OR") == 0){
		if (n->hi->data->ty != TBOOL || n->hd->data->ty != TBOOL){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion OR -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"AND") == 0){
		if (n->hi->data->ty != TBOOL || n->hd->data->ty != TBOOL){
			printf("Error semantico: el tipo de los elementos no es compatible con la operacion AND -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
		detect_error(n->hi);
	}
	if (strcmp(n->label,"NEG") == 0){
		if (n->hd->data->ty != TBOOL){
			printf("Error semantico: el tipo del elemento no es compatible con la operacion NEG -> Linea: %d \n", n->data->line);
			exit(1);
		}
		detect_error(n->hd);
	}
	if (strcmp(n->label, "IF") == 0){
		if (n->hi->data->ty != TBOOL) {
			printf("Error semantico: se requiere una expresion booleana. IF -> Linea: %d \n", n->hi->data->line);
			exit(1);
		}
		detect_error(n->hi);
		detect_error(n->hd);
	}

	if (strcmp(n->label, "WHILE") == 0){
		if (n->hi->data->ty != TBOOL) {
			printf("Error semantico: se requiere una expresion booleana. WHILE -> Linea: %d \n", n->hi->data->line);
			exit(1);
		}
		detect_error(n->hi);
		detect_error(n->hd);
	}

	if (strcmp(n->label, "CALLFUN") == 0){
		if (n->data->tinfo != TFUN){
			printf("Error: Esta funcion no esta definida -> Linea: %d \n", n->data->line);
			exit(1);
		}
		checkParam(n);
	}
	if (strcmp(n->label, "FUNC") == 0){
		checkParamForm(n);
		checkReturn(n);
		detect_error(n->hd);
	}
	return 0;
}

int show_tree(node* n,int i){
	if (n == NULL)
		return 0;
	for (int k = 0; k < i; ++k){
		printf("   │");
	}
	if ((strcmp(n->label, "CONS") == 0) || (strcmp(n->label, "BOOL") == 0))
		printf("── %d \n", n->data->val);
	else if (strcmp(n->label,"VAR") == 0)
		printf("── %s (%p) (%d) \n", n->data->name, n->data, n->data->ty);
	else if (strcmp(n->label,"FUNC") == 0)
		printf("── %s: %s \n", n->label, n->data->name);
	else if (strcmp(n->label,"PARAM") == 0){
		printf("── %s: ", n->label);
		printParam(n->data->p);
	}else if (strcmp(n->label,"CALLFUN") == 0){
		printf("── %s: %s \n", n->label, n->data->name);
	}
	else
		printf("── %s \n", n->label);
	show_tree(n->hi, i+1);
	show_tree(n->hd, i+1);
	return 0;
}

#endif

#ifndef _INSTRUC_C
#define _INSTRUC_C

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "table.h"
#include "instruc.h"

//Variable para el nombre de las temporales
static int id = 1;
static int labelJump = 1;
static int loadP = 1;

instruc* load_inst(instruc* anterior, inst i, info* op1, info* op2, info* op3){
	instruc* n = malloc(sizeof(instruc));
	anterior->sig = n;
	n->i = i;
	n->op1 = op1;
	n->op2 = op2;
	n->op3 = op3;
	return n;
}

info* createLabelLoadP(){
	info* temp = load_info_val(loadP, 0); 
	loadP = loadP+1;
	return temp;
}

//generador de variables temporales
info* createTempVar(){
	char *x=malloc(sizeof(char)*5);
	sprintf(x, "t%d", id);
	id = id+1;
	info* temp = load_info_name(x, 0); 
	temp->offset = get_offset();
	return temp;
}

//generador de label jump
info* createLabelJump(){
	info* temp = load_info_val(labelJump, 0); 
	labelJump = labelJump+1;
	return temp;
}

info* createLabelFun(char* namefun){
	char *x=malloc(sizeof(char)*5);
	sprintf(x, "%s", namefun);
	info* temp = load_info_name(x, 0);
	return temp;
}

info* createInfoOff(int off){
	info* temp = load_info_val(off, 0); 
	return temp;
}

instruc* createInstructLabel(enum inst ins, node* n, instruc* list){
	instruc* auxhi = NULL;
	instruc* auxhd = NULL;
	instruc* hhi = malloc(sizeof(instruc));
	instruc* hhd = malloc(sizeof(instruc));
	info* infi;
	info* infd;

	if ((strcmp(n->hi->label, "CONS") == 0) || (strcmp(n->hi->label, "BOOL") == 0) || (strcmp(n->hi->label, "VAR") == 0))
		infi = n->hi->data;
	else{
		auxhi = gen_list_instruc(n->hi, hhi);
		infi = auxhi->op3;
	}

	if ((strcmp(n->hd->label, "CONS") == 0) || (strcmp(n->hd->label, "BOOL") == 0) || (strcmp(n->hd->label, "VAR") == 0))
		infd = n->hd->data; 
	else{
		auxhd = gen_list_instruc(n->hd, hhd);
		infd = auxhd->op3;
	}
	
	if (auxhi != NULL){
		list->sig = hhi->sig;
		list = auxhi; 
	}
	if (auxhd != NULL){
		list->sig = hhd->sig;
		list = auxhd; 
	}

	return load_inst(list, ins, infi, infd, createTempVar());
}

instruc* genInstrucSaveParam(instruc* i, node* n){
	instruc* ix = i;
	node* nx = n;
	lpar* p = n->hi->data->p;
	while (p != NULL){
		ix = load_inst(ix, SAVEP, NULL, NULL, createLabelLoadP());
		p = p->sig;
	}
	loadP = 1;
	return ix;
}

instruc* gen_list_instruc(node* n, instruc* list){
	instruc* aux = malloc(sizeof(instruc));
	if((strcmp(n->label,"NEXT") == 0) || (strcmp(n->label,"GLOBAL") == 0) || (strcmp(n->label,"FUNCTIONS") == 0)){ //siguiente instruccion
		aux = list;
		if(n->hi != NULL){
			aux = gen_list_instruc(n->hi, list);
		}
		if(n->hd != NULL){
			aux = gen_list_instruc(n->hd, aux);
		}
		return aux;
	}

	if (strcmp(n->label, "EXTFUNC") == 0){
		return list;
	}

	if (strcmp(n->label, "DECL") == 0) {
		instruc* w = list;
		if (n->hi->data->tinfo == TGLOBAL)
			return load_inst(w, DECL, NULL, NULL, n->hi->data);
		return w;
	}

	if (strcmp(n->label,"DECLS") == 0){
		instruc* w = list;
		w = load_inst(w, GLOBAL, NULL, NULL, NULL);
		w = gen_list_instruc(n->hd, w);
		w = load_inst(w, ENDGLOBAL, NULL, NULL, NULL);
		return w;
	}

	if(strcmp(n->label, "RETURN") == 0){
		instruc* w;
		if ((strcmp(n->hd->label, "BOOL") == 0) || (strcmp(n->hd->label,"VAR") == 0) || (strcmp(n->hd->label,"CONS") == 0)){
			w = load_inst(list, RETURNN, n->hd->data, NULL, NULL);
		}else{
			w = gen_list_instruc(n->hd, list);
			w = load_inst(w, RETURNN, w->op3, NULL, NULL);
		}
		return w;
	}

	if(strcmp(n->label,"ASIG") == 0){ //asig HD null HI
		if ((strcmp(n->hd->label, "CONS") == 0) || (strcmp(n->hd->label, "BOOL") == 0) || (strcmp(n->hd->label,"VAR") == 0)){
			info* infd;
			infd = n->hd->data;
			return load_inst(list, ASIG, infd, NULL, n->hi->data);
		}else {
			instruc* auxhd = gen_list_instruc(n->hd, list);
			return load_inst(auxhd, ASIG, auxhd->op3, NULL, n->hi->data);
		}
	}

	if(strcmp(n->label,"PRINTI") == 0){
		if ((strcmp(n->hd->label, "CONS") == 0) || (strcmp(n->hd->label, "BOOL") == 0) || (strcmp(n->hd->label,"VAR") == 0)){
			info* infd;
			infd = n->hd->data;
			return load_inst(list, PRINT, infd, NULL, NULL);
		}

		instruc* auxhd = gen_list_instruc(n->hd, list);
		return load_inst(auxhd, PRINT, auxhd->op3, NULL, NULL);
	}

	if (strcmp(n->label,"SUMA") == 0){
		return createInstructLabel(SUMA, n, list);
	}

	if (strcmp(n->label,"RESTA") == 0){
		return createInstructLabel(RESTA, n,list);
	}

	if (strcmp(n->label,"MULT") == 0){
		return createInstructLabel(MULT, n, list);
	}

	if (strcmp(n->label,"DIV") == 0){
		return createInstructLabel(DIV, n, list);
	}

	if (strcmp(n->label,"MOD") == 0){
		return createInstructLabel(MOD, n, list);
	}

	if (strcmp(n->label,"MAYOR") == 0){
		return createInstructLabel(MAYOR, n, list);
	}

	if (strcmp(n->label,"MENOR") == 0){
		return createInstructLabel(MENOR, n, list);
	}

	if (strcmp(n->label,"OR") == 0){
		return createInstructLabel(ORR, n, list);
	}

	if (strcmp(n->label,"AND") == 0){
		return createInstructLabel(ANDD, n, list);
	}

	if (strcmp(n->label,"IGUAL") == 0){
		return createInstructLabel(EQ, n, list);
	}

	if (strcmp(n->label,"NEG") == 0){
		if ((strcmp(n->hd->label, "BOOL") == 0) || (strcmp(n->hd->label,"VAR") == 0)){
			return load_inst(list, NEG, n->hd->data , NULL, createTempVar());
		}
		instruc* a = gen_list_instruc(n->hd, list);
		return load_inst(a, NEG, a->op3, NULL, createTempVar());
	}

	if (strcmp(n->label, "IF") == 0){
		instruc* iff;
		info* label1 = createLabelJump();
		if ((strcmp(n->hi->label, "BOOL") == 0) || (strcmp(n->hi->label,"VAR") == 0) || (strcmp(n->hi->label,"CONS") == 0)){
			iff = load_inst(list, IFF, n->hi->data , NULL, label1);
		}else {
			iff = gen_list_instruc(n->hi, list);
			iff = load_inst(iff, IFF, iff->op3 , NULL, label1);
		}
		iff = load_inst(iff, THENN, NULL, NULL, NULL);
		iff = gen_list_instruc(n->hd->hi->hd, iff);
		if (n->hd->hd != NULL){
			info* label2 = createLabelJump();
			iff = load_inst(iff, GOTO, label2, NULL, NULL);
			iff = load_inst(iff, ENDTHEN, NULL, NULL, NULL);
			iff = load_inst(iff, ELSSE, label1, NULL, NULL);
			iff = gen_list_instruc(n->hd->hd->hd, iff);
			iff = load_inst(iff, ENDELSE, label2, NULL, NULL);
		}else
			iff = load_inst(iff, ENDTHEN, label1, NULL, NULL);
		return iff;
	}

	if (strcmp(n->label, "WHILE") == 0){
		instruc* w;
		info* labelRepeat = createLabelJump();
		info* labelEnd = createLabelJump();
		w = load_inst(list, LABEL, NULL, NULL, labelRepeat);
		if ((strcmp(n->hi->label, "BOOL") == 0) || (strcmp(n->hi->label,"VAR") == 0))
			w = load_inst(w, WHHILE, n->hi->data , NULL, labelEnd);
		else{
			w = gen_list_instruc(n->hi, w);
			w = load_inst(w, WHHILE, w->op3 , NULL, labelEnd);
		}
		w = gen_list_instruc(n->hd->hd, w);
		w = load_inst(w, ENDWHILE, NULL, labelRepeat, labelEnd);
		return w;
	}

	if (strcmp(n->label, "BLOCK") == 0){
		return 	gen_list_instruc(n->hd, list);
	}

	if (strcmp(n->label,"FUNC") == 0){
		instruc* w;
		info* labelEnd = createLabelJump();
		set_offset(n->data->offset);
		w = load_inst(list, FUN, n->data, NULL, NULL);
		instruc* aux = w;
		if (n->hi != NULL){
			w = genInstrucSaveParam(w, n); 
		}
		if (n->hd != NULL){
			w = gen_list_instruc(n->hd, w);
		}
		if (searchReturn(n) == NULL){
			if (n->data->ty == TBOOL){
				info* b = load_info_val(1, 0);
				b->ty = TBOOL;
				w = load_inst(w, RETURNN, b, NULL, labelEnd);
			}else{
				if (n->data->ty == TINT){
					info* b = load_info_val(0, 0);
					b->ty = TINT;
					w = load_inst(w, RETURNN, b, NULL, labelEnd);
				}	
			}
		}
		aux->op3 = createInfoOff(get_offset());
		while (aux != NULL){
			if (aux->i == RETURNN){
				aux->op3 = labelEnd;
			}
			aux = aux->sig;
		}
		w = load_inst(w, ENDFUN, n->data, NULL, labelEnd);
		return w;
	}

	if (strcmp(n->label,"MAIN") == 0){
		instruc* w;
		set_offset(n->data->offset);
		w = load_inst(list, MAINN, NULL, NULL, NULL);
		instruc* aux = w;
		w = gen_list_instruc(n->hd, w);
		w = load_inst(w, ENDMAIN, NULL, NULL, NULL);
		aux->op3 = createInfoOff(get_offset());
		return w;
	}

	if (strcmp(n->label,"CALLFUN") == 0){
		instruc* w;
		w = list;
		if (n->hd != NULL){
			table* tmp = malloc(sizeof(table));
			table* head = malloc(sizeof(table));
			head = tmp;
			node* aux = n->hd;
			while (aux->hi != NULL && aux->hd != NULL){
				if (strcmp(aux->hi->label, "CONS") == 0 || strcmp(aux->hi->label, "VAR") == 0 || strcmp(aux->hi->label, "BOOL") == 0){
					w = load_inst(w, ASIG, aux->hi->data, NULL, createTempVar());
				}
				else
					w = gen_list_instruc(aux->hi, w);
				tmp->data = w->op3;
				tmp->sig = malloc(sizeof(table));
				aux = aux->hd;
				tmp = tmp->sig;
			}
			if (aux->hi != NULL){
				if (strcmp(aux->hi->label, "CONS") == 0 || strcmp(aux->hi->label, "VAR") == 0 || strcmp(aux->hi->label, "BOOL") == 0){
					w = load_inst(w, ASIG, aux->hi->data, NULL, createTempVar());
				}
				else
					w = gen_list_instruc(aux->hi, w);
				tmp->data = w->op3;
				tmp->sig = malloc(sizeof(table));
				tmp = tmp->sig;
			}
			while(head->sig != NULL){
				w = load_inst(w, LOADPARAM, head->data, NULL, createLabelLoadP());
				head = head->sig;
			}
			loadP = 1;
		}
		w = load_inst(w, CALL, n->data, NULL, createTempVar());
		return w;
	}
	return NULL;
} 


void priti(instruc* list){
	while(list != NULL){
		if (list->i == CALL){
			printf("CALL ");
		}
		if (list->i == SUMA){
			printf("SUMA ");
		}
		if (list->i == RESTA){
			printf("RESTA ");
		}
		if (list->i == MULT){
			printf("MULT ");
		}
		if (list->i == DIV){
			printf("DIV ");
		}
		if (list->i == MOD){
			printf("MOD ");
		}
		if (list->i == ASIG){
			printf("ASIG ");
		}
		if (list->i == PRINT){
			printf("PRINT ");
		}
		if (list->i == EQ){
			printf("EQUAL ");
		}
		if (list->i == ORR){
			printf("OR ");
		}
		if (list->i == ANDD){
			printf("AND ");
		}
		if (list->i == MAYOR){
			printf("MAYOR ");
		}
		if (list->i == MENOR){
			printf("MENOR ");
		}	
		if (list->i == NEG){
			printf("NEG ");
		}
		if (list->i == IFF){
			printf("IF ");
		}
		if (list->i == THENN){
			printf("THEN ");
		}
		if (list->i == ENDTHEN){
			printf("ENDTHEN ");
		}
		if (list->i == ELSSE){
			printf("ELSE ");
		}
		if (list->i == ENDELSE){
			printf("ENDELSE ");
		}
		if (list->i == GOTO){
			printf("GOTO ");
		}
		if (list->i == LABEL){
			printf("LABEL ");
		}
		if (list->i == WHHILE){
			printf("WHILE ");
		}
		if (list->i == ENDWHILE){
			printf("ENDWHILE ");
		}
		if (list->i == FUN){
			printf("FUN ");
		}
		if (list->i == ENDFUN){
			printf("ENDFUN ");
		}
		if (list->i == MAINN){
			printf("MAIN ");
		}
		if (list->i == ENDMAIN){
			printf("ENDMAIN ");
		}
		if (list->i == LOADPARAM){
			printf("LOADPARAM ");
		}
		if (list->i == SAVEP){
			printf("SAVEPARAM ");
		}
		if (list->i == RETURNN){
			printf("RETURN ");
		}
		if (list->i == GLOBAL){
			printf("GLOBAL ");
		}
		if (list->i == ENDGLOBAL){
			printf("ENDGLOBAL ");
		}
		if (list->i == DECL){
			printf("DECL ");
		}
		if (list->op1 == NULL){
			printf("_ ");
		}else{
			if (list->op1->name != NULL){
				printf("%s ", list->op1->name);
			}
			else{
				printf("%d ", list->op1->val);
			}
		}
		if (list->op2 == NULL){
			printf("_ ");
		}else{
			if (list->op2->name != NULL){
				printf("%s ", list->op2->name);
			}else{
				printf("%d ", list->op2->val);
			}
		}
		if (list->op3 == NULL){
			printf("_ ");
		}else{
			if (list->op3->name != NULL){
				printf("%s ", list->op3->name);
			}else{
				printf("%d ", list->op3->val);
			}
		}
		
		printf("\n");
		list = list->sig;
	}
}

#endif
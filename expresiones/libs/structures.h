#ifndef _STRUCTURES_H
#define _STRUCTURES_H

typedef enum typ {TINT, TBOOL}type;

typedef enum fvp {TFUN, TVAR, TPARAM, TGLOBAL}fvp;

typedef struct lpar{
	char* name;
	enum typ ty; //type
	struct lpar* sig;
}lpar;

typedef struct info{
	char* name;
	int val;
	int line;
	enum typ ty; //type
	int offset;
	enum fvp tinfo;
	lpar* p;
}info;


typedef struct table{
	struct table* sig;
	info* data;
}table;

typedef struct stack{
	int level;
	struct table* simb;
	struct stack* sig;
}stack;

typedef struct node{
	struct node* hi;
	struct node* hd;
	char* label;
	info* data;
}node;

typedef enum inst {ASIG, SUMA, RESTA, MULT, DIV, MOD, CALL, PRINT, MAYOR, MENOR, EQ, ORR, ANDD, NEG, IFF, THENN, ENDTHEN, ELSSE, ENDELSE, LABEL,  WHHILE, ENDWHILE, GOTO, FUN, ENDFUN, MAINN, ENDMAIN, LOADPARAM, SAVEP, RETURNN, GLOBAL, ENDGLOBAL, DECL}inst;

typedef struct instruc{
	enum inst i;
	info* op1;
	info* op2;
	info* op3;
	struct instruc* sig;
}instruc;

#endif
#ifndef _INSTRUC_H
#define _INSTRUC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "structures.h"


instruc* load_inst(instruc* n, inst i, info* op1, info* op2, info* op3);

info* createLabelJump();

info* createTempVar();

instruc* createInstructLabel(enum inst ins, node* n, instruc* list);

instruc* gen_list_instruc(node* n, instruc* list);

instruc* createIntrucParam(node* n, instruc* list);

instruc* genInstrucSaveParam(instruc* i, node* n);

void priti(instruc* list);

#endif
#ifndef _GENASSEMBLY_H
#define _GENASSEMBLY_H

#include <stdio.h>
#include <stdlib.h>
#include "instruc.h"

int generator_init(FILE *arch, instruc* list);

int gen_file(instruc* list);

int generator_end(FILE* archivo);

char* createNameJmp(int label);

int assemblyOperations(FILE* archive, instruc* list);

#endif
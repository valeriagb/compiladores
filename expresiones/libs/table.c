#ifndef _TABLE_C
#define _TABLE_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "table.h"

//Variable para manejar el offset del frame
static int offs = 0;

int reset_offset(){
	offs = 0;
	return offs;
}

int get_offset(){
	offs = offs - 8;
	return offs;
}

int set_offset(int n){
	offs = n;
	return offs;
}

info* load_info_val(int v, int line){
	info* i = malloc(sizeof(info));
	i->val = v;
	i->line = line;
	return i;
}

info* load_info_name(char* name, int line){
	info* i = malloc(sizeof(info));
	i->name = name;
	i->line = line;
	return i;
}

info* update_val(info* i, int v){
	if (i != NULL){
		i->val = v; //actualiza valor variable
	} else{
		printf("ERROR --> linea %d : Variable %s no definida.\n", i->line, i->name);
		exit(1);
	}
	return i;
}


table* search_in_list(table* head, info* id, int bool){
	table* aux = head;
	while (aux != NULL){
		if(strcmp(aux->data->name,id->name) != 0){
			aux = aux->sig;
		}
		else{
			return aux;
		}
	}
	if (bool){
		if(aux == NULL){
			if (id->tinfo == TVAR || id->tinfo == TGLOBAL){
				printf("ERROR --> linea %d : Variable %s no definida.\n", id->line, id->name);
				exit(1);
			}else{
				if (id->tinfo == TFUN){
					printf("ERROR --> linea %d : Funcion %s no definida.\n", id->line, id->name);
					exit(1);
				}
			}
		}
	}
	return aux;
}

table* insert_in_list(table* head, info* id){
	table* a = search_in_list(head, id, 0);
	if(a != NULL){
		if (a->data->tinfo == TVAR || a->data->tinfo == TGLOBAL){
			printf("ERROR --> linea %d : Existe ya definida una variable %s.\n", id->line, id->name);
			exit(1);
		}else {
			if (a->data->tinfo == TFUN){
				printf("ERROR --> linea %d : Existe ya definida una funcion %s.\n", id->line, id->name);
			exit(1);
			} 
		}
	}
	table* l = malloc(sizeof(table));
	l->sig = head;
	l->data = id;
	return l;
}

int printTable(table* head){
	if (head == NULL){
		printf("NULL \n");
		return 0;
	}
	printf("%s -> ", head->data->name);
	printTable(head->sig);
	return 0;
}

#endif
#ifndef _STACK_H
#define _STACK_H

#include <stdlib.h>
#include <stdio.h>
#include "structures.h"

stack* getLevel(stack* st, int level);

stack* insertLevel(stack* st);

stack* deleteLevel(stack* st);

table* searchInStack(stack* stk, info* v, int bool);

stack* insertInLevels(stack* stk, info* v);

stack* insertParamInLevel(stack* s, lpar* p);

#endif
#ifndef _PARAM_C
#define _PARAM_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "table.h"

lpar* load_param(lpar* p, char* name){
	p->name = name;
	return p;
}

int printParam(lpar* h){
	char* ty;
	if (h->ty == 0)
		ty = "int";
	else
		ty = "bool";
	printf("(%s %s) ", ty, h->name);
	if (h->sig != NULL){
		printf(" -> ");
		printParam(h->sig);
	}else
		printf(" \n");
	return 0;
}

#endif

#ifndef _TREE_H
#define _TREE_H

#include <stdlib.h>
#include <stdio.h>
#include "structures.h"

struct node* load_node(node* n, node* sl, node* sr, info* d, char* l);

int checkParamForm(node* n);

node* searchReturn(node* n);

int detect_error(node* n);

int checkParam(node* n);

int checkReturn(node* n);

int show_tree(node* n, int i);

#endif
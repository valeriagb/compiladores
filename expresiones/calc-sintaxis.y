%{

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libs/param.h"
#include "libs/stack.h"
#include "libs/table.h"
#include "libs/tree.h"
#include "libs/structures.h"
#include "libs/instruc.h"
#include "libs/genAssembly.h"

stack* simb;

%}

%union {char *s; struct node* nod; int t; lpar* par;}

%token<nod> INT
%token<nod> TRUE
%token<nod> FALSE
%token<s> IF
%token<s> THEN
%token<s> ELSE
%token<s> WHILE
%token<s> AND
%token<s> OR
%token<s> NG
%token<s> EQUAL
%token<s> MAIN
%token<s> EXTERN
%token<s> RETURN
%token<s> VAR
%token<t> BOOL
%token<t> INTEGER
%token<nod> PRINTI
%token<nod> PRINTB
%token<nod> ID
%type<nod> lexpr
%type<nod> expr
%type<nod> prog
%type<nod> program
%type<nod> block
%type<nod> body
%type<nod> declaration
%type<nod> ldecl
%type<nod> statements
%type<nod> statement
%type<nod> func
%type<nod> lfun
%type<par> param
%type<par> lparam
%type<t> type

%left AND
%left OR 
%nonassoc '<' '>' EQUAL
%left '+' '-' 
%left '*' '/' '%' 
%left UMINUS

%%

prog: program { 
                printf("------------------------------------------------------------------------\n");
                detect_error($1);
                printf("Arbol del programa: \n");
                show_tree($1, 0);
                printf("------------------------------------------------------------------------\n");
                instruc* ksi = malloc(sizeof(instruc));
                gen_list_instruc($1, ksi);
                gen_file(ksi->sig);
                priti(ksi->sig);
                printf("------------------------------------------------------------------------\n");
              }
 
program: {simb = insertLevel(simb);} ldecl lfun {
                                    node* d = load_node(malloc(sizeof(node)), NULL, $2, NULL, "DECLS");
                                    node* f = load_node(malloc(sizeof(node)), NULL, $3, NULL, "FUNCTIONS");
                                    node* n = load_node(malloc(sizeof(node)), d, f, NULL, "GLOBAL");
                                    simb = deleteLevel(simb); 
                                    $$ = n;
                                  };
          |{simb = insertLevel(simb);} lfun {
                                    node* f = load_node(malloc(sizeof(node)), NULL, $2, NULL, "FUNCTIONS");
                                    node* n = load_node(malloc(sizeof(node)), NULL, f, NULL, "GLOBAL");
                                    simb = deleteLevel(simb); 
                                    $$ = n;};

param : type ID { 
                  $2->data->ty = $1;
                  $2->data->tinfo = TPARAM;
                  $2->data->offset = get_offset();
                  simb = insertInLevels(simb, $2->data);
                  lpar* p = load_param(malloc(sizeof(lpar)), $2->data->name);
                  p->ty = $2->data->ty;
                  $$ = p;
                };

lparam:   param ',' lparam {$1->sig = $3;
                            $$ = $1;}
        | param { $$ = $1;};

func:  EXTERN INTEGER ID '('  {    
                                  simb = insertLevel(simb);} lparam ')' ';' {
                                  $3->data->p = $6;
                                  $3->label = "FUN";
                                  node* pr = load_node(malloc(sizeof(node)), NULL, NULL, malloc(sizeof(info)),"PARAM");
                                  pr->data->p = $6;
                                  node* n = load_node(malloc(sizeof(node)), pr, NULL, $3->data, "EXTFUNC");
                                  simb = deleteLevel(simb); 
                                  simb = insertInLevels(simb, $3->data);
                                  $$ = n;
                                 };
     
      | EXTERN INTEGER ID '(' ')' ';' { $3->label = "FUN";
                                        node* n = load_node(malloc(sizeof(node)), NULL, NULL, $3->data, "EXTFUNC");
                                        simb = insertInLevels(simb, $3->data);
                                        $$ = n;
                                      };
      | type ID '(' {simb = insertLevel(simb);reset_offset();} lparam ')' block {
                                      $2->data->ty = $1;
                                      $2->data->p = $5;
                                      $2->label = "FUN";
                                      $2->data->tinfo = TFUN;
                                      node* pr = load_node(malloc(sizeof(node)), NULL, NULL, malloc(sizeof(info)),"PARAM");
                                      pr->data->p = $5;
                                      int line = $2->data->line;
                                      info* dn = load_info_name($2->data->name, line);
                                      dn->ty = $1;
                                      dn->p = $5;
                                      dn->tinfo = TFUN;
                                      dn->offset = get_offset();
                                      node* n = load_node(malloc(sizeof(node)), pr, $7, dn, "FUNC");
                                      simb = deleteLevel(simb); 
                                      simb = insertInLevels(simb, $2->data);
                                      $$ = n;
                                     };

      | type ID '(' ')'  {reset_offset();} block {
                                $2->data->ty = $1;
                                $2->label = "FUN";
                                $2->data->tinfo = TFUN;
                                node* n = load_node(malloc(sizeof(node)), NULL, $6, $2->data, "FUNC");
                                n->data->offset = get_offset();
                                simb = insertInLevels(simb, $2->data);
                                $$ = n;
                              };

lfun:  func lfun {
                    node* n = load_node(malloc(sizeof(node)), $1, $2, NULL, "NEXT");
                    $$ = n;
                  };

      |INTEGER MAIN '(' ')' block {
                                    reset_offset();
                                    info* m = malloc(sizeof(info));
                                    m->name = "main";
                                    m->ty = TINT;
                                    m->offset = get_offset(); //max offset
                                    node* n = load_node(malloc(sizeof(node)), NULL, $5, m, "MAIN");
                                    $$ = n;
                                   };

block : '{' {simb = insertLevel(simb);} body '}' {
                        node* n = load_node(malloc(sizeof(node)), NULL, $3, NULL, "BLOCK");
                        simb = deleteLevel(simb); 
                        $$ = n;
                       }
        | '{' '}' { 
                    $$ = load_node(malloc(sizeof(node)), NULL, NULL, NULL, "BLOCK_EMPTY");
                  }
        ;

body:    statements {
                        $$ = $1;
                       }
        | declaration ';' body  {
                                  $$ = load_node(malloc(sizeof(node)), $1, $3, NULL, "NEXT");
                                }
        | {$$=NULL;}
    ;

ldecl:  declaration ';' ldecl  {
                                 $1->hi->data->tinfo = TGLOBAL;
                                 $$ = load_node(malloc(sizeof(node)), $1, $3, NULL, "NEXT");
                              };
       | declaration ';' {
                          $1->hi->data->tinfo = TGLOBAL;
                          $$ = $1;
                          };
 
declaration:  VAR type ID   {
                              $3->data->offset = get_offset();
                              $3->data->ty = $2; 
                              $3->data->tinfo = TVAR;
                              $3->label = "VAR";
                              simb = insertInLevels(simb, $3->data);
                              node* nod = load_node(malloc(sizeof(node)), $3, NULL, malloc(sizeof(info)), "DECL");
                              nod->data->ty = $2;
                              nod->data->line = $3->data->line;
                              $$ = nod;
                            }
            | VAR type ID '=' expr  { $3->data->offset = get_offset();
                                      $3->data->ty = $2; 
                                      $3->data->tinfo = TVAR;
                                      $3->label = "VAR";
                                      simb = insertInLevels(simb, $3->data);
                                      node* nod = load_node(malloc(sizeof(node)), $3, $5, malloc(sizeof(info)), "ASIG");
                                      nod->data->line = $3->data->line;
                                      nod->data->ty = $2;
                                      $$ = nod;
                                    }

    ;

statements:   statement statements {
                                    $$ = load_node(malloc(sizeof(node)), $1, $2, NULL, "NEXT");
                                   }
            | statement {
                          $$ = $1;
                        }
          ;

statement: ID '=' expr ';' {
                          table* l = searchInStack(simb,$1->data,1);
                          node* n = load_node(malloc(sizeof(node)), NULL, NULL, l->data, "VAR");
                          node* nod = load_node(malloc(sizeof(node)), n, $3, malloc(sizeof(info)), "ASIG");
                          nod->data->ty = $1->data->ty;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }
          | IF '(' expr ')' THEN block {
                                        node* bodyIf = load_node(malloc(sizeof(node)), $6, NULL, NULL, "BODYIF");
                                        node* n = load_node(malloc(sizeof(node)), $3, bodyIf, malloc(sizeof(info)), "IF");
                                        $$ = n;
                                       }
          | IF '(' expr ')' THEN block ELSE block {
                                                    node* bodyIf = load_node(malloc(sizeof(node)), $6, $8, NULL, "BODYIF");
                                                    node* n = load_node(malloc(sizeof(node)), $3, bodyIf, malloc(sizeof(info)), "IF");
                                                    int line = $3->data->line;
                                                    n->data->line = line;
                                                    $$ = n;
                                                  }
          | WHILE '(' expr ')' block {
                                      node* n = load_node(malloc(sizeof(node)), $3, $5, malloc(sizeof(info)), "WHILE");
                                      $$ = n;
                                     }
          | PRINTI '(' expr ')' ';'{
                                  $$ = load_node(malloc(sizeof(node)), NULL, $3, NULL, "PRINTI");
                                }

          | RETURN expr ';' {
                              $$ = load_node(malloc(sizeof(node)), NULL, $2, NULL, "RETURN");
          }

          | block { $$=$1;}

          |';' {
                $$ = load_node(malloc(sizeof(node)), NULL, NULL, NULL, "SKIP");
               }


          ;

type: INTEGER           {
                          $$ = $1;
                        }
    | BOOL              {
                          $$ = $1;
                        }

lexpr: expr ',' lexpr {
                        node* nod = load_node(malloc(sizeof(node)), $1, $3, NULL, "NEXTPARAM");
                        $$ = nod;
                      };
      | expr { node* nod = load_node(malloc(sizeof(node)), $1, NULL, NULL, "NEXTPARAM");
              $$ = nod;
              };

expr:
     expr EQUAL expr   {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "IGUAL");
                          nod->data->ty = TBOOL;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr OR expr      {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "OR");
                          nod->data->ty = TBOOL;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr AND expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "AND");
                          nod->data->ty = TBOOL;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | NG expr %prec UMINUS{
                          node* nod = load_node(malloc(sizeof(node)), NULL, $2, malloc(sizeof(info)), "NEG");
                          nod->data->ty = TBOOL;
                          int line = $2->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }
    |  expr '+' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "SUMA");
                          nod->data->ty = TINT;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr '-' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "RESTA");
                          nod->data->ty = TINT;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr '*' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "MULT");
                          nod->data->ty = TINT;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr '/' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "DIV"); 
                          nod->data->ty = TINT;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr '%' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "MOD"); 
                          nod->data->ty = TINT;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr '<' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "MENOR");
                          nod->data->ty = TBOOL;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | expr '>' expr     {
                          node* nod = load_node(malloc(sizeof(node)), $1, $3, malloc(sizeof(info)), "MAYOR");
                          nod->data->ty = TBOOL;
                          int line = $1->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }


    | '(' expr ')'      {
                          $$ = $2;
                        }

    | '-' expr %prec UMINUS {
                          info* i = malloc(sizeof(info));
                          i->val = -1;
                          node* n = load_node(malloc(sizeof(node)), NULL, NULL, i, "CONS");
                          node* nod = load_node(malloc(sizeof(node)), $2, n, malloc(sizeof(info)), "MULT");
                          nod->data->ty = TINT;
                          int line = $2->data->line;
                          nod->data->line = line;
                          $$ = nod;
                        }

    | ID '(' lexpr ')'   {
                          table* l = searchInStack(simb,$1->data,1);
                          node* nod = load_node(malloc(sizeof(node)), NULL, $3, l->data, "CALLFUN");
                          nod->data->line = $1->data->line;
                          $$ = nod;
                         }
    
    | ID '(' ')'        {
                          table* l = searchInStack(simb,$1->data,1);
                          node* nod = load_node(malloc(sizeof(node)), NULL, NULL, l->data, "CALLFUN");
                          int line = $1->data->line;
                          nod->data->line = line; 
                          $$ = nod;
                        }


    | ID                {
                          table* l = searchInStack(simb,$1->data,1);
                          node* n;
                          n = load_node(malloc(sizeof(node)), NULL, NULL, l->data , "VAR");
                          int line = $1->data->line;
                          n->data->line = line;
                          $$ = n;
                        }
    | INT               { 
                          $1->data->ty = TINT;
                          $$ = $1;
                        }
    
    | FALSE             {
                          $1->data->ty = TBOOL;
                          $$ = $1;
                        }

    | TRUE              {
                          $1->data->ty = TBOOL;
                          $$ = $1;
                        }
    ;

%%

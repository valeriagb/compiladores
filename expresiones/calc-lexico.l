%{

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "libs/structures.h"
#include "libs/table.h"
#include "libs/tree.h"
#include "calc-sintaxis.tab.h"

%}

%option noyywrap
%option yylineno

letra [a-zA-Z]
digito [0-9]

%%

{digito}+                     { 
								yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, load_info_val(atoi(yytext), yylineno), "CONS");
								yylval.nod->data->line = yylineno;
                                return INT;
                              }

"main"						  {	
								return MAIN;
							  }

"false"						  {
								yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, load_info_val(0, yylineno), "BOOL");
								yylval.nod->data->line = yylineno;
								return FALSE;
							  }
							  
"true"						  {
								yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, load_info_val(1, yylineno), "BOOL");
								yylval.nod->data->line = yylineno;
								return TRUE;
							  }

"var"						  { 
								return VAR;
							  } 

"printi"					  { 
								return PRINTI;
							  }

"printb"					  { 
								return PRINTI;
							  }

"int"					  {
								yylval.t = TINT;
								return INTEGER;
							  }

"bool"					  	  {
								yylval.t = TBOOL;
								return BOOL;
							  }

"if"						  {
								return IF;
							  }

"then"						  {
								return THEN;
							  }

"else"						  {
								return ELSE;
							  }

"while"						  {
								return WHILE;
							  }

"extern"					  {
								return EXTERN;
							  }

"return"					  {
								return RETURN;
							  }

"!"							  {
								return NG;
							  }	
							  
"&&"						  {
								return AND;
							  }

"||"						  {
								return OR;
							  }

"=="						  {
								return EQUAL;
							  }

{letra}({letra}|{digito})*    { 
								char* aux = strcpy(malloc(sizeof(yytext)*yyleng), yytext);
								yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, 
								load_info_name(aux, yylineno), NULL);
                                return ID;
                              }

[-+*/=%<=>;(){}]|","          {  //printf("%s\n",yytext);
                                  return *yytext;}
"//".*\n            		  {}
	                              

.|\n                          ; /* ignore all the rest */

%%

void yyerror(){
	printf("%s%d\n","-> ERROR Sintactico en la linea: ",yylineno);
}

int main(int argc,char *argv[]){
	++argv,--argc;
	if (argc > 0)
		yyin = fopen(argv[0],"r");
	else
		yyin = stdin;

	yyparse();

}

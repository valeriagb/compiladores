# Taller de Diseño de Software (3306)
### Proyecto (año 2019)
* ***García, Valeria*** 🛠️


## _Aclaraciones Generales_  📋
* 📌 *Se adjunta carpeta **libs**, que contiene librerias creadas para representar y manipular la tabla de simbolos y el arbol.*
* 📌 *Dentro de **libs**, en el archivo *structures.h*, se definen las siguientes estructuras:*
	* 🔧 **info:** utilizado para almacenar, en caso de ser una variable, su nombre, su valor y el numero de linea de la definicion. En caso de ser una constante numerica, su nombre es NULL.
	* 🔧 **table:** representa la tabla de simbolos, donde se almacena un tipo **info** y un puntero al siguiente elemento de la tabla.
	* 🔧 **node:** representa cada nodo del arbol. Cada uno de ellos contiene dos Nodos hijos y ademas almacena un **label**(1) señalando el tipo del nodo y un tipo **info**.
	* 🔧 **instruc:** representa una instruccion del codigo de 3 direcciones. Cada una de estas estructuras contiene el nombre de la instruccion, 3 punteros tipo **info** que representan los operadores de la instruccion y un puntero que apunta a la siguiente instruccion.
	* 🔧 **stack:** representa la pila de niveles para identificar la vida temporal de las variables.
	* 🔧 **lpar:** representa la lista de paramentros formales de las funciones al momento de su definicion.
* 📌 *El arbol se muestra iniciando con un nodo raiz, seguido primero por su hijo izquierdo y luego por su hijo derecho.*

## _Instrucciones de Compilacion y Ejecucion_  📋
* 📌 *Para la compilacion se debe ejecutar el archivo **scritp**.*
* 📌 *Para testear *
	* 🔧  *ejecutar la linea: **./a.out test/(archivo)**.*
	Por consola podemos observar el arbol y **el codigo de tres direccciones(2)** de nuestro archivo de texto de prueba.
	Ademas, genera el archivo con codigo assembler **a.s**.
* 📌 *Luego de realizar el cualquiera de los dos pasos anteriores, debemos compilar el codigo assembler generado, ejecutando la linea **gcc a.s libsTests/(libreria)**.*
* 📌 *Para finalizar, comprobar que el ejecutable generado realice las acciones correspondiente a nuestras instrucciones ejecutando **./a.out**.*


(1) **Tablas con referencias de label:**

|  		label 	  |   Referencia   |   Hijo Izquierdo  |    Hijo Derecho   |
|-----------------|----------------|-------------------|-------------------|
| 	 "GLOBAL"     | prog completo  |   decl globales   |  def de funciones |
| 	 "DECLS"      | decl globales  |    declaracion    | NEXT / declaracion|
| 	"FUNCTIONS"   | def funciones  |   	  funcion      |  	NEXT / MAIN    |
| 	 "EXTFUNC"	  |  func externa  |   	NULL / param   | 		NULL 	   |
| 	  "FUNC"	  | def de funcion |   	NULL / param   |  cuerpo de func   |
| 	  "MAIN"	  |	prog principal |   		NULL  	   |    cuerpo main    |
| 	  "NEXT"      | salto de linea |    linea actual   |  siguiente linea  |
| 	  "BLOCK"     | bloque entre {}|      NULL  	   | cuerpo del bloque |
| 	  "DECL"      |  decl(sin val) |nombre de variable | 		NULL	   |
| 	  "ASIG"      |   asignacion   |nombre de variable |  valor a asignar  |
| 	  "SUMA"      |      suma 	   | exp a la izq de + | exp a la der de + |
| 	  "RESTA"     |     resta 	   | exp a la izq de - | exp a la der de - |
| 	  "MULT"      | multiplicacion | exp a la izq de * | exp a la der de * |
| 	   "DIV"      |    division    | exp a la izq de / | exp a la der de / |
| 	   "MOD"      |     modulo     | exp a la izq de % | exp a la der de % |
| 	   "VAR"      |   variable	   |	   NULL		   |       NULL		   |
| 	  "PRINTI"    | func imprimir  |       NULL        |   exp a imprimir  |
| 	  "RETURN"    |  inst de ret   |       NULL        |   exp a retornar  |
| 	 "CALLFUN"    | llamada a fun  |       NULL        |    NULL / param   |
| 	  "MAYOR"     |   func mayor   |	 1° operador   |    2°operador     |
| 	  "MENOR"     |   func menor   |	 1° operador   |    2°operador     |
| 	   "EQ"       |   func igual   |	 1° operador   |    2°operador     |
| 	   "OR"       |   func  or 	   |	 1° operador   |    2°operador     |
|      "AND       |    func and    |	 1° operador   |    2°operador     |
|  	   "NEG"      | func negacion  |	 NULL		   |     operador      |
|  	   "IF"       | condicional IF |	 condicion     |   cuerpo del IF   |
|  	 "BODYIF"     |  cuerpo de IF  |  cuerpo del THEN  | NULL / cuerpo ELSE|
|  	  "WHILE"     |   ciclo WHILE  |	 condicion     |  cuerpo del WHILE |


(2) **Tablas con referencias de codigo de 3 direcciones:**

|  	OPERACION     |   	1° OP 	   |   	  2° OP 	   |    	3 OP 	   |
|-----------------|----------------|-------------------|-------------------|
| 	   "ASIG"     |   valor a asig |		NULL	   |a quien se le asig |
| 	   "SUMA"     |  1° valor a +  |	 2° valor a +  |  donde se guarda  |
| 	   "RESTA"    |  1° valor a -  |	 2° valor a -  |  donde se guarda  |
| 	   "MULT"     |  multiplicando | 	multiplicador  |  donde se guarda  |
| 	   "DIV"      |    dividendo   |  	   divisor     |  donde se guarda  |
| 	   "MOD"      |    dividendo   |  	   divisor     |  donde se guarda  |
| 	  "PRINTI"    |valor a imprimir|       NULL        |   	   NULL		   |
| 	  "MAYOR"     |	 1° operador   |    2°operador     |  donde se guarda  |
| 	  "MENOR"     |	 1° operador   |    2°operador     |  donde se guarda  |
| 	   "EQ"       |	 1° operador   |    2°operador     |  donde se guarda  |
|  	   "OR"       |	 1° operador   |    2°operador     |  donde se guarda  |
|      "AND"      |	 1° operador   |    2°operador     |  donde se guarda  |
|  	   "NEG"      |valor a imprimir|       NULL        |   	   NULL		   |
|  	   "IF"       |    condicion   |	   NULL        |   label de salto  |
|  	  "THEN"      |     NULL 	   |	   NULL        |   	   NULL        |
|    "ENDTHEN"    |     NULL 	   |	   NULL        |   	   NULL        |
|  	  "GOTO"      | label de salto |	   NULL        |       NULL        |
|  	  "ELSE"      | label de salto |	   NULL        |       NULL        |
|  	 "ENDELSE"    | label de salto |	   NULL        |       NULL        |
|  	  "WHILE"     |    condicion   |	   NULL        |   label de salto  |
|   "ENDWHILE"    |	    NULL       |  label de salto   |   label de salto  |
|     "MAIN"      |     NULL 	   |	   NULL        |   	max Offset     |
|    "ENDMAIN"    |     NULL 	   |	   NULL        |   	   NULL        |
|    "GLOBAL"     |     NULL 	   |	   NULL        |   	   NULL        |
|   "ENDGLOBAL"   |     NULL 	   |	   NULL        |   	   NULL        |
|  	   "FUN"      |  name function |	   NULL        |	max Offset     |
|  	 "ENDFUN"     |  name function |	   NULL        |	   NULL 	   |
|   "LOADPARAM"   |  param actual  |	   NULL        |   n° param formal |
|     "SAVEP"     | 	 NULL	   |	   NULL        |   n° param formal |
|     "CALL"   	  |  name funcion  |  	   NULL        |  donde se guarda  |
|     "RETURN"    | exp a retornar |  	   NULL        |  label de salto   |

